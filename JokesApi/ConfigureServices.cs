using JokesApi.Application.Services;
using JokesApi.Domain.Common.Interfaces;
using JokesApi.Infrastructure.Persistence;
using JokesApi.Infrastructure.Persistence.Interceptors;
using JokesApi.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;

namespace Microsoft.Extensions.DependencyInjection;

public static class ConfigureServices
{
    public static IServiceCollection AddServices(this IServiceCollection services,
        IConfiguration configuration)
    {

        services.AddTransient<IDateTime, DateTimeService>();
        services.AddScoped<AuditableEntitySaveChangesInterceptor>();

        var connectionString = configuration["ASPNETCORE_ENVIRONMENT"] == "Development"
            ? configuration.GetConnectionString("DefaultConnection")
            : configuration["DB_CONNECTION_STRING"];

        services.AddDbContext<ApplicationDbContext>(options =>
        {
            options.UseNpgsql(
                connectionString,
                opts => opts.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName)
            );
        });

        services.AddScoped<IApplicationDbContext>(prov => prov.GetRequiredService<ApplicationDbContext>());

        services.AddScoped<ApplicationDbContextInitialiser>();
        services.AddSingleton<IRemoteJokesService, RemoteJokesService>();

        return services;
    }
}