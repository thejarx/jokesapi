namespace JokesApi.Domain.Entities;

// Auditable record
public record Joke : BaseAuditableEntity
{
    public int Id { get; set; }
    public string Content { get; set; }
}
