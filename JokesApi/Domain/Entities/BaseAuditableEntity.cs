namespace JokesApi.Domain.Entities;

public record BaseAuditableEntity
{
  public DateTime CreatedAt { get; set; }
  public DateTime UpdatedAt { get; set; }
}