using System.Reflection;
using Microsoft.EntityFrameworkCore;
using JokesApi.Domain.Entities;

namespace JokesApi.Domain.Common.Interfaces;

public interface IApplicationDbContext
{
    DbSet<Joke> Jokes { get; }

    Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
}
