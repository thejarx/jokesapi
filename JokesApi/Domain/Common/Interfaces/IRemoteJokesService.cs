using JokesApi.Domain.Entities;

namespace JokesApi.Domain.Common.Interfaces
{
  public interface IRemoteJokesService
  {
    Task<Joke> GetDadJokeAsync();
    Task<Joke> GetChuckNorrisJokeAsync();
    Task<Joke> GetRandomJokeAsync();
  }
}