using System.Text.Json;
using JokesApi.Domain.Common.Interfaces;

var builder = WebApplication.CreateBuilder(args);

builder.Configuration.AddEnvironmentVariables();
builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddServices(builder.Configuration)
.ConfigureHttpJsonOptions(options =>
{
    options.SerializerOptions.PropertyNamingPolicy = JsonNamingPolicy.CamelCase;
});


// No Authentication or Authorisation implemented yet
var app = builder.Build();
var jokesRouter = app.MapGroup("/jokes");

app.MapGet("/coolboi", () => TypedResults.Ok("😎"));
app.MapGet("/health", () => TypedResults.Ok("Healthy!"));

// I like to use handlers for my endpoints, but for this simple app I'll just make
jokesRouter.MapGet("/", async (IRemoteJokesService remoteJokesService) =>
{
    var joke = await remoteJokesService.GetRandomJokeAsync();
    return TypedResults.Ok(joke);
});

jokesRouter.MapGet("/chuck", async (IRemoteJokesService remoteJokesService) =>
{
    var joke = await remoteJokesService.GetChuckNorrisJokeAsync();
    return TypedResults.Ok(joke);
});

jokesRouter.MapGet("/dad", async (IRemoteJokesService remoteJokesService) =>
{
    var joke = await remoteJokesService.GetDadJokeAsync();
    return TypedResults.Ok(joke);
});

app.Run();
