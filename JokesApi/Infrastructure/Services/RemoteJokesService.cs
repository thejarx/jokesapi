using System.Collections.Specialized;
using JokesApi.Domain.Common.Interfaces;
using JokesApi.Domain.Entities;
using RestWrapper;

namespace JokesApi.Infrastructure.Services;
record ChuckNorrisJoke
{
  public string value { get; init; }
}
record DadJoke
{
  public string joke { get; init; }
}

public class RemoteJokesService : IRemoteJokesService
{
  private readonly string _chuckNorrisUrl = "https://api.chucknorris.io/jokes/random";
  private readonly string _dadJokeUrl = "https://icanhazdadjoke.com/";
  public async Task<Joke> GetChuckNorrisJokeAsync()
  {
    var req = new RestRequest(_chuckNorrisUrl);

    RestResponse response = await req.SendAsync();
    var rawJoke = response.DataFromJson<ChuckNorrisJoke>();

    // I'd use a proper mapper here, but I'm lazy
    return new Joke
    {
      Content = rawJoke.value
    };
  }

  public async Task<Joke> GetDadJokeAsync()
  {
    NameValueCollection headers = new()
    {
        { "Accept", "application/json" }
    };

    var req = new RestRequest(_dadJokeUrl, HttpMethod.Get, headers, "application/json");

    RestResponse response = await req.SendAsync();
    var rawJoke = response.DataFromJson<DadJoke>();

    return new Joke
    {
      Content = rawJoke.joke
    };
  }

  public async Task<Joke> GetRandomJokeAsync()
  {
    var random = new Random();
    var jokeType = random.Next(0, 2);

    return jokeType switch
    {
      0 => await GetChuckNorrisJokeAsync(),
      1 => await GetDadJokeAsync(),
      _ => throw new Exception("Something went wrong") // just in case
    };
  }
}