using JokesApi.Domain.Common.Interfaces;

namespace JokesApi.Application.Services;

public class DateTimeService : IDateTime
{
    public DateTime Now => DateTime.UtcNow;
}