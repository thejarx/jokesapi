using Xunit;
using JokesApi.Domain.Common.Interfaces;
using JokesApi.Infrastructure.Services;

namespace JokesApi.UnitTests.Services.RemoteJokesServices
{
  public class GetChuckNorrisJokeAsyncShould
  {
    private readonly IRemoteJokesService _remoteJokesService;

    public GetChuckNorrisJokeAsyncShould()
    {
      _remoteJokesService = new RemoteJokesService();
    }

    [Fact]
    public async Task ReturnAJoke()
    {
      var joke = await _remoteJokesService.GetChuckNorrisJokeAsync();
      Assert.NotNull(joke);
      Assert.NotEmpty(joke.Content);
    }
  }
}