using Xunit;
using JokesApi.Domain.Common.Interfaces;
using JokesApi.Infrastructure.Services;

namespace JokesApi.UnitTests.Services.RemoteJokesServices
{
  public class GetDadJokeAsyncShould
  {
    private readonly IRemoteJokesService _remoteJokesService;

    public GetDadJokeAsyncShould()
    {
      _remoteJokesService = new RemoteJokesService();
    }

    [Fact]
    public async Task ReturnAJoke()
    {
      var joke = await _remoteJokesService.GetDadJokeAsync();
      Assert.NotNull(joke);
      Assert.NotEmpty(joke.Content);
    }
  }
}